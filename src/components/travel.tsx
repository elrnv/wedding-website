import * as React from 'react'
import Header from './header'
import { ScheduleRightSection } from './schedule'
import Footer from './footer'
// import React, { useState, useEffect } from 'react'

import {
  Main,  
  UpperContent,
  QuestionsPageContainer,
  Address,
  AddressLabel
} from './styled'

import { 
  QuestionContainer, 
  QuestionImage} from './qastyled'

const TravelLayout = () => {
  return (
    <Main >
      <Header/>
      <UpperContent>
        <QuestionsPageContainer>
          <QuestionContainer>
            <QuestionImage src="/wickinn.png" />
            <br/>
            <br/>
            <br/>
            <AddressLabel
              href="https://www.wickinn.com/">Wickaninnish Inn</AddressLabel>
            <Address
              href="https://maps.app.goo.gl/3DMo5MGEchAzKUyb9">
              500 Osprey Lane
              <br/>
              Tofino, BC VoR 2Zo      
            </Address>
          </QuestionContainer>
          <QuestionContainer>
        
          <ScheduleRightSection 
              title="Getting around the area"
              addressTitle=""
              addressLine1=""
              addressLine2=""
              addressUrl=""
              attire=""
              details={
                "Car rental: Rent a car if you would like to explore the region during your visit. It is recommended to book this in advance. \n\nBikes: For those staying at the inn, the Wickaninnish Inn offers complimentary bike use on a first-come, first-serve basis. There is a paved path that goes from all the way from Tofino to Ucluelet. \n\nWickaninnish Inn Shuttle: The Inn offers limited complimentary daytime shuttle service between Tofino and the Inn. Contact the inn or visit the Concierge to arrange this in advance. \n\nWhistle! Ride Share app: This app is similar to Uber or Lyft. You'll need to download the app through your phone's app store. \n\nWest Coast Transit: there are buses that run between Tofino to Ucluelet from 7am to 7pm. It is a cashless system and passes are required to be purchased in advance. More details can be found here at www.tofino.ca/public-services/transportation/ "}
            />
          </QuestionContainer>
        </QuestionsPageContainer>
      </UpperContent>
      <Footer/>
    </Main>
  )
}

export default TravelLayout
