import * as React from 'react'
import Header from './header'
import { ScheduleRightSection } from './schedule'
import Footer from './footer'
// import React, { useState, useEffect } from 'react'

import {
  Main,  
  UpperContent,
  QuestionsPageContainer,
  // Address,
  SingleActivity,
  AddressLabel,
  ScheduleDetailItemParagraph,
  SingleActivityContainer, 
} from './styled'

import { QuestionImage } from './qastyled'

const ActivitiesLayout = () => {
  return (
    <Main >
      <Header/>
      <UpperContent>
        <QuestionsPageContainer>
          <QuestionImage src="/dine.png" />
            <br/>
            <br/>
          <br/>
          <SingleActivityContainer>
            <ScheduleRightSection 
              title="Dining"
              addressTitle=""
              addressLine1=""
              addressLine2=""
              addressUrl=""
              attire=""
              details=""
            />
          </SingleActivityContainer>
          <SingleActivityContainer>
            <SingleActivity
                href="https://www.pluvio.ca/">
                Pluvio Restaurant in Ucluelet
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Rated the best destination restaurant in Canada. Chef Warren Barr (who use to be executive chef at the Pointe at Wickinn) offers creative and inspired Canadian cuisine focused on seasonal and wild foraged ingredients. Intimate vibes and open kitchen. Reservations are strongly recommended
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://thepointerestaurant.ca/">
              The Pointe Restaurant at Wickaninnish Inn
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Fine dining with panoramic 240-degree vistas of the Pacific Ocean from seasonal à la carte menu to seasonal testing menu. The restaurant boasts an extensive wine selection curated to complement the cuisine. Reservations are highly recommended. Dress code is smart casual
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
                href="https://wolfinthefog.com/?v=7516fd43adaa">
                Wolf in the Fog
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Sea view resetaurant with warm, rustic vibe serving locally sourced plates and cocktails 
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://www.tacofino.com/">
              Tacofin
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Delicious tacos from a taco truck, a must visit if you like tacos
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/ki1Jb4mDMXi551339">
              Jeju Restaurant
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Culinary journey showcasing rich and bold flavours of modern Korean cuisine. Offers imported Koreal and locally sourced ingredients
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/gpm4rdGA2MMrgPG39">
              Shelter Restaurant
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Waterfront restaurant and brewery serving local cuisine and seafood. Vegetarian friendly.
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/aDqHz642hu2XVmXLA">
              The Schooner Restaurant
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Comfortable refined eatery for steak and seafood. Unique cocktails and local bears
              </ScheduleDetailItemParagraph>
              <br/><br/>
          </SingleActivityContainer>
          
          <SingleActivityContainer>
            <ScheduleRightSection 
                title="Cafés & Breakfast"
                addressTitle=""
                addressLine1=""
                addressLine2=""
                addressUrl=""
                attire=""
                details=""
              />
          </SingleActivityContainer>
          <SingleActivityContainer>
            <SingleActivity
              href="https://maps.app.goo.gl/AEexz1959XvTeFnJA">
              Rhino Coffee House
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Coffee from freshly roasted beans. All day breakfast, sandwiches, wraps, and salads.
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/ydP2bfMYbtb7P3hn6">
              Roar
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Smoked salmon, pork belly, or friend chicken bennys, waffles and french toast. All the breakfast goodies for a weekend brunch
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/Mg7SxmdGTRsgYKiB7">
              Tofino Coffee Roasting Company
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Locally owned and operated roastery and cafe. In house made pastries. 
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/3D8L2ey9qj1eEucz6">
              Driftwood Café at Wickaninnish Inn
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Bakery, breakfast, coffee, and some morning bubbles next to the beach and rainforest
              </ScheduleDetailItemParagraph>
              <br/><br/>
          </SingleActivityContainer>
          <SingleActivityContainer>
            <ScheduleRightSection 
                title="Relax"
                addressTitle=""
                addressLine1=""
                addressLine2=""
                addressUrl=""
                attire=""
                details=""
              />
          </SingleActivityContainer>
          <SingleActivityContainer>
            <SingleActivity
              href="https://www.wickinn.com/ancient-cedars-spa/">
              Ancient Cedars Spa at Wickaninnish Inn
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Reserve in advance. 
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://tuffcitysaunas.com/">
              Tuft City Saunas
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Two locations to choose from by the ocean. Bookings accepted one month in advance. 
              </ScheduleDetailItemParagraph>
              <br/><br/>
          </SingleActivityContainer>
          <SingleActivityContainer>
            <ScheduleRightSection 
                title="Shopping, Arts, and Culture"
                addressTitle=""
                addressLine1=""
                addressLine2=""
                addressUrl=""
                attire=""
                details=""
              />
          </SingleActivityContainer>
          <SingleActivityContainer>
            <SingleActivity
              href="https://www.wickinn.com/about/henry-nollas-carving-shed/">
              The Carving Shed at Wickaninnish Inn
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Beautiful wood carvings
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/zkE6sMqCyF2PnaEs7">
              Roy Henry Vickers Gallery
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                First Nations print maker
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/Ci1KzoACe9RrAVMB8">
              Jeremy Koreski Gallery
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Photographer
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/QRjfy7RqSp5Wx9ecA">
              Merge Curated Goods
            </SingleActivity>
              <ScheduleDetailItemParagraph>
              Gift Shop
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/Th4unYnAEkMhCNbA6">
              Caravan Beach Shop
            </SingleActivity>
              <ScheduleDetailItemParagraph>
              Gift Shop
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://maps.app.goo.gl/dcNzL2GC1eUPFMSg6">
              The Den
            </SingleActivity>
              <ScheduleDetailItemParagraph>
              Gift Shop
              </ScheduleDetailItemParagraph>
              <br/><br/>
          </SingleActivityContainer>
          <SingleActivityContainer>
            <ScheduleRightSection 
                title="Movement"
                addressTitle=""
                addressLine1=""
                addressLine2=""
                addressUrl=""
                attire=""
                details=""
              />
          </SingleActivityContainer>  
          <SingleActivityContainer>
            <SingleActivity
              href="https://tourismtofino.com/things-to-do/activities-adventures/cycling/">
              Biking
            </SingleActivity>
              <ScheduleDetailItemParagraph>
              Rent bikes and ride them down the beach or on the paved multi use path from Tofino to Ucluelet. For folks staying at the inn, bikes are provided on a first come basis at Wickaninnish Inn. 
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://tourismtofino.com/things-to-do/activities-adventures/cycling/">
              Hiking
            </SingleActivity>
              <ScheduleDetailItemParagraph>
              Nature walks and ample hikes, too many to choose from. 
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://www.tofinohiking.com/hikes/">
              Explore the Beaches
            </SingleActivity>
              <ScheduleDetailItemParagraph>
              The closest beach to the hotel is Chesterman beach, explore tidepools at low tide near the rocks. Cox bay beach is great to watch people surf or surf yourself. Long Beach is the longest beach in Vancouver Island with beautiful views. Mackenzie Beach is known for calmer waters, gentle waves, and mostly sheltered from wind if you're looking for a family friendly experience. 
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://tourismtofino.com/things-to-do/activities-adventures/kayaking-canoeing/">
              Kayaking / Canoeing
            </SingleActivity>
              <ScheduleDetailItemParagraph>
              Kayak and canoe rentals are available near Tofino Harbour. Tofino is home to a number of experienced guides. A good start is the two and half hour tour of the harbour and its islands. 
              </ScheduleDetailItemParagraph>
              <br/><br/>
            <SingleActivity
              href="https://tourismtofino.com/things-to-do/activities-adventures/fishing/">
              Fishing
            </SingleActivity>
              <ScheduleDetailItemParagraph>
              Offshore salmon, halibut, lingcod, rockfish, prawns, and Dungeness crabs. Fishing charters are available and scheduled for 5 to 10 hour days. 
              </ScheduleDetailItemParagraph>
              <br/><br/>
          </SingleActivityContainer>
          
          <SingleActivityContainer>
            <ScheduleRightSection 
                title="Longer Activities"
                addressTitle=""
                addressLine1=""
                addressLine2=""
                addressUrl=""
                attire=""
                details=""
              />
          </SingleActivityContainer>  
          <SingleActivity
              href="https://hikebiketravel.com/broken-group-islands/">
              Broken Group Backcountry Sea Kayaking
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                The Broken Group is a cluster of around 100 forested and rocky islands off the west coast of Vancouver Island. Planning is required in advance to book campsites and kayak rentals. This was one of our favorite multi-day camping trips. Please let us know if you'd like more details.
              </ScheduleDetailItemParagraph>
              <br/><br/>
          <SingleActivity
              href="https://hikebiketravel.com/broken-group-islands/">
              Hot Springs Cove
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Arrive via a 1.5 hour boat ride or seaplane, walk a 1.5km boardwalk through old growth forest, and relax in the hot springs. 
              </ScheduleDetailItemParagraph>
              <br/><br/>
          <SingleActivity
              href="https://tuffcitysaunas.com/">
              Remote Floating Sauna & Dock
            </SingleActivity>
              <ScheduleDetailItemParagraph>
                Private floating sauna experience nested deep within UNESCO Biosphere Reserve. Experience includes travel by boat, kayaks and paddleboards, and foraged crab and prawns.
              </ScheduleDetailItemParagraph>
              <br/><br/>
          
          
        </QuestionsPageContainer>

      </UpperContent>
      <Footer/>
    </Main>
  )
}

export default ActivitiesLayout
