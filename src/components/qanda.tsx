import * as React from 'react'
import Header from './header'
import { ScheduleRightSection } from './schedule'
import Footer from './footer'
// import React, { useState, useEffect } from 'react'

import {
  Main,
  UpperContent,
  QuestionsPageContainer
} from './styled'

import { QuestionContainer, MapImage } from './qastyled'

const QALayout = () => {
  return (
    <Main >
      <Header/>
      <UpperContent>
        <QuestionsPageContainer>
          <br/>
          <MapImage src="/wickinn-map.png" />
          <br/><br/>
          <QuestionContainer>
            <ScheduleRightSection 
              title="Contact Information"
              addressTitle=""
              addressLine1=""
              addressLine2=""
              addressUrl=""
              attire=""
              details={"We'll be here for you throughout the trip! Save our number and don't hesitate to call or text us if you have any questions or need anything at all. \n\nDian: 628-220-9545 \nEgor: 206-600-8894"}
            />
            </QuestionContainer>
          <QuestionContainer>
            
          <ScheduleRightSection 
              title="What is the dress code?"
              addressTitle=""
              addressLine1=""
              addressLine2=""
              addressUrl=""
              attire=""
              details="The dress code for the wedding is formal. There are no color preferences, wear what makes you feel beautiful or handsome :). Please refer to the schedule for detailed dress codes for pre-wedding activities."
            />
          </QuestionContainer>
          
          <QuestionContainer>
          <ScheduleRightSection 
              title="What's the weather like?"
              addressTitle=""
              addressLine1=""
              addressLine2=""
              addressUrl=""
              attire=""
              details={"It's almost summer, but we're also on the Pacific West. Average temperatures during the day average from low 50s to low 70s on this particular weekend. Due to the temperate climate, rain can occur at any time, so bring some rain gear! Blankets will be provided at the ceremony. See schedule for more details on rain plan."}
            />
          </QuestionContainer>

          <QuestionContainer>
          <ScheduleRightSection 
              title="What should I pack?"
              addressTitle=""
              addressLine1=""
              addressLine2=""
              addressUrl=""
              attire=""
              details={"Nature is dynamic, and we will be in the midst of it! The west coast is known for its rugged coastline shaped by the elements. Tofino area is in a temperate rainforest, meaning even the hottest days can cool with sea breezes in the evening. \n\nClothes: Comfortable clothes for exploring! Layers, long sleeves, short sleeves, sweaters, shorts, pants, and socks. Bring a warm layer even when it feels like a warm day as the weather can change throuhgout the day. Workout clothes would be great if you're doing yoga on the beach or hikes with us. Comfy clothes for the evening in front of the fireplace. If you're daring to go into the ocean, pack a swim suit! \n\nShoes: Good pair of hiking or walking shoes. Options for great hikes or long walks are endless. Slippers or sandals for the beach and spa. \n\nIf you are staying at the Wickaninnish Inn, they provide rain gear, rain boots, backpacks, bottles, beach towels, and other items for excursions. Please inquire with the hotel for availability."}
            />
          </QuestionContainer>
          <QuestionContainer>
            <ScheduleRightSection 
              title="What will be served at dinner?"
              addressTitle=""
              addressLine1=""
              addressLine2=""
              addressUrl=""
              attire=""
              details={"We will be working with the executive chef at the Pointe Restaurant and the culinary team to create a custom 5-course fine dining menu for our group. Wine pairings are available selected by the in-house sommelier. \n\nPlease let us know if you have any dietary restrictions. We also have a special kid's menu available for children under 12."}
            />
            </QuestionContainer>
          <QuestionContainer>
            <ScheduleRightSection 
              title="Will there be a bar at the reception?"
              addressTitle=""
              addressLine1=""
              addressLine2=""
              addressUrl=""
              attire=""
              details="Drinks are on us! The reception will feature a hosted bar offering delightful selection of wines, beers, premium spirits, and crafted cocktails. For the adventurous guest seeking an even wider variety or a personalized drink experience, the On the Rocks bar is conveniently located steps away from our space."
            />
          </QuestionContainer>
          <QuestionContainer></QuestionContainer>
          <QuestionContainer>
            <ScheduleRightSection 
              title="Will there be a steamer or iron?"
              addressTitle=""
              addressLine1=""
              addressLine2=""
              addressUrl=""
              attire=""
              details="The hotel has some irons and 1 steamer. We will be bringing an additional steamer that you can also use."
            />
            </QuestionContainer>
            
        </QuestionsPageContainer>
      </UpperContent>
      <Footer/>
    </Main>
  )
}

export default QALayout
