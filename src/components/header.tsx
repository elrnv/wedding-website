import * as React from 'react'
import Menu from './menu'
import {
  Banner,
  Nav,
  Logo,
  BANNER_MAX,
  MOBILE,
} from './styled'

const Header = () => {
  const [windowSize, setWindowSize] = React.useState<{ width: number, height: number }>();

  const handleResize = React.useCallback(() => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  }, []);

  React.useEffect(() => {
    window.addEventListener('resize', handleResize, false);
    handleResize();
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  if (windowSize === undefined) {
    return <></>
  }

  let hideLogo = windowSize.width < BANNER_MAX;
  let smallBanner = windowSize.width < MOBILE;

  if (!hideLogo) {
    return (
      <Nav>
        <Banner href="/"><Logo src="/favicon.svg" />DIAN & EGOR</Banner>
        <Menu></Menu>
      </Nav>
    )
  } else if (!smallBanner) {
    return (
      <Nav>
        <Banner>DIAN & EGOR</Banner>
        <Menu></Menu>
      </Nav>
    )
  } else {
    return (
      <Nav>
        <Banner>D & E</Banner>
        <Menu></Menu>
      </Nav>
    )
  }
}

export default Header