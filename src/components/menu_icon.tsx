import * as React from 'react'
import styled from "styled-components";
import {
  COLOR_BLACK,
} from './styled'

const MenuIconButton = styled.button`
top: 2rem;
right: 0rem;
display: flex;
flex-direction: column;
float: right;
margin-right: -4px;
justify-content: space-around;
width: 52px;
height: 40px;
background: transparent;
border: none;
cursor: pointer;
z-index: 5;

div {
    width: 40px;
    height: 1.9px;
    background: ${COLOR_BLACK};
    border-radius: 5px;
    transform-origin: 1px;
}
`

const MenuIcon = (props: any) => {
    return (
        <div>
        <MenuIconButton onClick={props.onClick}>
        <div />
        <div />
        <div />
        </MenuIconButton>
        </div>
    )
}

export default MenuIcon