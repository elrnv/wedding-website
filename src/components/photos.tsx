import * as React from 'react'
import Header from './header'
import { ScheduleRightSection } from './schedule'
import Footer from './footer'
// import React, { useState, useEffect } from 'react'

import {
  Main,  
  UpperContent,
  QuestionsPageContainer,
  PhotosContainer,
  PhotosImage
} from './styled'

import { 
  QuestionContainer, 
  QuestionImage} from './qastyled'

const PhotosLayout = () => {
  return (
    <Main >
      <Header/>
      <UpperContent>
          <PhotosContainer>
            <PhotosImage src="/photo.png" />
            <PhotosImage src="/photos5.png" />
            <PhotosImage src="/photos2.png" />
            <PhotosImage src="/photos3.png" />
            <PhotosImage src="/photos4.png" />
            <PhotosImage src="/photos6.png" />
            <PhotosImage src="/photos7.png" />
          </PhotosContainer>
      </UpperContent>
      <Footer/>
    </Main>
  )
}

export default PhotosLayout
