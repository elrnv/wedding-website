import * as React from 'react'
import {
  ScheduleTitle,
  ScheduleDetail
} from './styled'

import {
    Address,
    NotFoundContainer
} from './styled'

const NotFound = () => {
  return (
    <NotFoundContainer>
        <ScheduleTitle>
            PAGE NOT FOUND
        </ScheduleTitle>
        
        <ScheduleDetail>
        Uh oh, it looks like you took a wrong turn <br/>
        This page doesn't exist 😔
        </ScheduleDetail>
        
        <Address href="/">Go to home page</Address>
    </NotFoundContainer>
  )
}

export default NotFound