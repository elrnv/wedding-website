import styled from "styled-components";
  
// Header
export const QuestionContainer = styled.div`
    margin-bottom: 2.5em;
    position: center;
`;

export const QuestionImage = styled.img`
    width: 100%;
    position: center;
    margin-top: 0.6em;
    border-radius: 2em;
`

export const MapImage = styled.img`
width: 100%;
position: center;
margin-bottom:12px;
border-radius: 2em;
`