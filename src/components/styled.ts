import styled from "styled-components";

export const BACKGROUND_COLOR = `rgba(255, 250, 244, 1)`;
export const COLOR_BLACK = `#37474E`
export const COLOR_ORANGE = `#FF6B4A`
export const COLOR_GREEN = `rgba(92, 111, 106, 1)`
export const MENU_MAX: number = 1150;
export const BACKGROUND_MAX: number = 880;
export const BANNER_MAX: number = 500;
export const MOBILE: number = 400;

const background = require("../../static/dian-egor.png").default;
// const backgroundNoLocation = require("./smallbackground.svg").default;

const size = {
    mobile: `${MOBILE}px`,
    tablet: '612px',
    background: `${BACKGROUND_MAX}px`,
    laptop: '1480px',
    desktop: '2560px',
    detailflex: '976px',
    menumax: `${MENU_MAX}px`
}

const device = {
    mobileandbigger: `(min-width: ${size.mobile})`,
    tabletandbigger: `(min-width: ${size.tablet})`,
    backgroundandbigger: `(min-width: ${size.background})`,
    laptopandbigger: `(min-width: ${size.laptop})`,
    desktop: `(min-width: ${size.desktop})`,
    detailflexandsmaller: `(max-width: ${size.detailflex})`,
    menumaxandbigger: `(min-width: ${size.menumax})`,
};

// Header
export const Nav = styled.nav`
    position: fixed;
    background-color: ${BACKGROUND_COLOR};
    width: 100%;
    z-index: 5;
`;
export const HorizontalMenu = styled.ul`
    float: right;
    list-style-type: none;
    margin: 22px;
    margin-right: 20px;
    padding: 0;
    overflow: hidden;
    display: flex;
`;

export const HamburgerMenu = styled.div`
    float: right;
    margin: 30px;
    margin-right: 20px;
    overflow: hidden;
    display: flex;
    flex-direction: column;
`;

export const VerticalMenu = styled.ul<{ $isVisible: boolean }>`
    float: right;
    list-style-type: none;
    margin: 0;
    padding: 0;
    padding-top: ${props => props.$isVisible
        ? "20px"
        : "0px"
    };
    overflow: hidden;
    flex-direction: column;
    display: flex;
    height: ${props => props.$isVisible
        ? "100%"
        : "0px"
    };
`;

export const NavigationItem = styled.li`
    font-size: 20px;
    padding-top: 16px;
    padding-bottom: 16px;
    padding-left: 26px;
    padding-right: 6px;
    text-align: right;
    
    font-weight: 300;
    opacity: 100%;
    color: ${COLOR_BLACK};
    text-decoration: none;
    text-underline-offset: 6px;
    &: hover {
        text-decoration: underline;
    }
`;

export const Logo = styled.img`
    height: 22px;
    margin-top: 8px;
    margin-right: 15px;
    @media ${device.tabletandbigger} {
        height: 30px;
        margin-top: 11px;
        margin-right: 20px;
    }
`

export const Banner = styled.a`
    position: absolute;
    z-index: 10;
    font-size: 30px;
    margin-left: 20px;
    margin-top: 31px;
    margin-right: 0px;
    // @media ${device.mobileandbigger} {
    //     font-size: 32px;
    //     margin-top: 31px;
    //     margin-right: 0px;
    // }
    @media ${device.tabletandbigger} {
        font-size: 40px;
        margin-top: 26px;
        margin-right: 0px;
    }
    // margin-left: 3%;
    text-align: left;
    letter-spacing: 6px;
    font-weight: 300;
    opacity: 100%;
    display: flex;
    float: left;
    color: ${COLOR_BLACK};
`;

// Footer 
export const Bottom = styled.div`
    text-align: center;
    margin-top: 200px;
    // margin-left: 3%;
    // margin-right: 3%;
    margin-bottom: 100px;
    color: ${COLOR_GREEN};
    font-family: Lato;
    font-size: 40px;
    font-style: normal;
    font-weight: 300;
    line-height: normal;
    opacity: 85%;
    letter-spacing: 6px;
`;

// Schedule
export const Schedule = styled.div`
`;

// export const BackgroundImage = styled.div<{ $imageOpacity: number; }>`
//     content: "";
//     background-image: url(${background});
//     background-repeat: no-repeat;
//     // background-size: auto;
//     background-attachment: fixed;
//     background-position: top right;        
//     opacity: ${props => props.$imageOpacity};
//     // position: relative;
//     // top: 0px;
//     right: 0px;
//     // bottom: 0px;
//     // left: 0px;
//     // z-index: -1;
//     height: 963px;
//     width: 100%;
//     position: absolute;
//     background-color: ${BACKGROUND_COLOR};

//     @media ${device.backgroundandbigger} {
//         background-image: url(${background});   
//     }
// `;

export const BackgroundImage = styled.img<{ $imageOpacity: number; $pos: boolean }>`
    opacity: ${props => props.$imageOpacity};
    position: absolute;
    // top: 104px;
    right: 0px;
    // bottom: 0px;
    // left: 0px;
    z-index: -1;
    // height: 50%;
    max-height: 963px;
    overflow: clip;
    // width: 100%;
    // max-width:
    // background-color: ${BACKGROUND_COLOR};
`;

export const Main = styled.div`
    position: relative;
    font-family: Lato;
    color: ${COLOR_BLACK};
    flex-direction: column;
    overflow: auto;
    width: 100%;
    margin: 0;
    z-index: 1;
    background-color: ${BACKGROUND_COLOR};
`;

const Paragraph =
    `font-size: 16px;
font-family: Lato;
// letter-spacing: 2.8px;
line-height: 30px;

@media ${device.tabletandbigger} {
    font-size: 18px;
    line-height: 32px;
    
}
@media ${device.laptopandbigger} {
    font-size: 20px;
    line-height: 36px;
}`

export const HotelSnippet = styled.p`
    ${Paragraph}
    margin-top: 52px;
    text-align: center;
    // margin-left: 3%;
    // margin-right: 3%;
    margin-bottom: 200px;
    font-weight: 400;
    max-width: 300px;
    
    @media ${device.tabletandbigger} {
        max-width: 466px;
    }
    @media ${device.laptopandbigger} {
        margin-top: 68px;
        max-width: 512px;  
    }
`;

export const SaveTheDate = styled.p`
    font-size: 16px;
    @media ${device.mobileandbigger} {
        font-size: 20px;
    }
    @media ${device.tabletandbigger} {
        font-size: 24px;
    }
    
    color: ${COLOR_GREEN};
    letter-spacing: 3.6px;
    font-weight: 400;
    margin-top: 215px;
`;

export const DateBanner = styled.div<{ $posRelative: boolean }>`
    position: ${props => props.$posRelative ? "relative" : "absolute"};
    margin-top: 48px;
    margin-left: 20px;
`;

export const Date = styled.h1`
    // margin: 48px 3%;
    margin: 0px;
    letter-spacing: 5.0px;
    // padding: 0px 36px;
    line-height: 150%;
    font-size: 32px;
    // letter-spacing: 11.1px;
    @media ${device.menumaxandbigger} {
        font-size: 48px;
        letter-spacing: 11.1px;
    }
    @media ${device.laptopandbigger} {
        font-size: 64px;
        letter-spacing: 11.1px;
    }
    
    color: ${COLOR_BLACK};
    font-family: 'Poiret One';
    font-style: normal;
    font-weight: 400;
    // -webkit-text-stroke: 1px ${COLOR_BLACK};
    
`;

export const AddressSection = styled.div`
    margin-top: 20px;
    // margin: auto 3%;
`
export const AddressLabel = styled.a`
    font-family: Lato;
    color: ${COLOR_BLACK};
    display: block;
    font-weight: 700;
    text-transform: uppercase;
    // line-height: normal;
    text-decoration:none;
    cursor: pointer;

    font-size: 16px;
    letter-spacing: 3px;
    margin-bottom: 8px;
    
    @media ${device.tabletandbigger} {
        font-size: 20px;
        margin-bottom: 8px;
    }
    
`;

export const SingleActivityContainer = styled.div`
    margin-bottom: 2em;
    position: center;
`;

export const SingleActivity = styled.a`
    font-family: Lato;
    display: block;
    color: ${COLOR_ORANGE};
    font-weight: 400;
    text-decoration:none;
    cursor: pointer;

    font-size: 16px;
    letter-spacing: 3px;
    margin-bottom: 8px;
    line-height: 150%;
    
    @media ${device.tabletandbigger} {
        font-size: 20px;
        margin-bottom: 16px;
    }
`;

export const Address = styled.a`
    font-family: Lato;
    display: block;
    color: ${COLOR_ORANGE};
    font-weight: 400;
    text-decoration:none;
    cursor: pointer;

    font-size: 16px;
    letter-spacing: 3px;
    // margin-bottom: 8px;
    line-height: 200%;
    
    @media ${device.tabletandbigger} {
        font-size: 20px;
        margin-bottom: 16px;
    }
`;

export const UpperContent = styled.div<{ $height?: number }>`
    position: relative;
    text-align: left;
    // letter-spacing: 3px;
    display: inline-block;
    margin-top: 92px;
    width: 100%;
    height: ${props => props.$height ? `${props.$height}px` : "100%"};
`;

export const NotFoundContainer = styled.div`
    margin: 88px 12px 66px 66px;
`;



// Schedule Details 

export const COLOR_GREEN_DIVIDER = `rgba(92, 111, 106, 0.8)`

export const ScheduleContainer = styled.div`
    position: relative;
    margin-bottom: 200px;
`
export const ScheduleTitle = styled.p`
    margin-bottom: 20px;
    font-family: Poiret One;
    font-size: 24px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    letter-spacing: 5.2px;
    @media ${device.tabletandbigger} {
        font-size: 48px;
        letter-spacing: 7.2px;
    }
`;

export const ScheduleDate = styled.p`
    color: ${COLOR_BLACK};
    font-family: Lato;
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    letter-spacing: 2.6px;
    margin-bottom: 16px;
    @media ${device.tabletandbigger} {
        font-size: 24px;
        letter-spacing: 3.6px;
    }
`;

export const ScheduleDetail = styled.div`
    display: flex;
    // flex-wrap: wrap;
    margin-top: 15px;

    @media ${device.detailflexandsmaller} {
        text-align: left;
        display: block;
    }
    @media ${device.tabletandbigger} {
        margin-top: 48px;
        font-size: 24px;
        letter-spacing: 3.6px;
    }
    // ${Paragraph}
    // color: ${COLOR_BLACK};
    // font-family: Lato;
    // font-size: 20px;
    // font-style: normal;
    // font-weight: 400;
    // line-height: 36px; /* 180% */
    // letter-spacing: 3px;
`;

export const ScheduleDivider = styled.hr`
    position: relative;
    width: 100%;
    color: ${COLOR_GREEN_DIVIDER};
    opacity: 85%;
    border: none;
    height: 1px;
    background-color: ${COLOR_GREEN_DIVIDER};
    z-index: -2;
`;

export const ScheduleItemContainer = styled.div`
    margin: 20px;
    margin-top: 56px;
    // 3%
`;

export const QuestionsPageContainer = styled.div`
    margin: 0px 20px 36px 20px;
    // margin-top: 56px;
    // 3%
`;

export const PhotosContainer = styled.div`
    margin: 0.6em 20px 36px 20px;
    position: flex;
    // margin-top: 56px;
    // 3%
`;

export const PhotosImage = styled.img`
    width: 100%;
    margin: auto;
    margin-bottom: 1%;
    border-radius: 2em;
`



export const ScheduleDetailTime = styled.div`
    font-family: Lato;
    color: ${COLOR_BLACK};
    margin-right:26px;
    font-size: 16px;
    letter-spacing: 2.6px;
    @media ${device.tabletandbigger} {
        font-size: 20px;
        letter-spacing: 3.6px;
    }
`
export const ScheduleDetailItem = styled.div`
    font-family: 'Poiret One';
    font-size: 20px;
    letter-spacing: 2.4px;
    font-weight: 400;
    color: ${COLOR_GREEN};
    @media ${device.tabletandbigger} {
        font-size: 32px;
        letter-spacing: 2.4px;
    }
`

export const ScheduleDetailItemTitle = styled.a`
    font-family: Lato;
    color: ${COLOR_BLACK};
    font-weight: 700;
    text-transform: uppercase;
    text-decoration:none;
    font-size: 12px;
    letter-spacing: 1.8px;
    margin: 0px;
    line-height: normal
    
    @media ${device.tabletandbigger} {
        font-size: 14px;
        letter-spacing: 3px;
    }
`;

export const ScheduleDetailItemAddress = styled.a`
    font-family: Lato;
    color: ${COLOR_ORANGE};
    font-weight: 400;
    text-decoration:none;
    cursor: pointer;
    font-size: 14px;
    line-height: 150%;
    letter-spacing: 1.8px;
    margin-bottom: 4px;
    display: block;
    
    @media ${device.tabletandbigger} {
        font-size: 17px;
        letter-spacing: 2px;
    }
`;

export const ScheduleDetailItemParagraph = styled.p`
    // font-variant-ligatures: none;
    font-family: Lato;
    white-space: pre-wrap;
    color: ${COLOR_BLACK};
    font-weight: 300;
    text-decoration:none;
    // cursor: pointer;
    max-width: 600px;
    margin: 0px;
    line-height: 150%;
    font-size: 14px;
    letter-spacing: 1.8px;
    // letter-spacing: 3px;
    // margin-bottom: 12px;
    
    @media ${device.tabletandbigger} {
        font-size: 17px;
        letter-spacing: 2px;
    }
`;

export const ScheduleRight = styled.div`
    position: flex;
    margin-bottom: 36px;
    
`;

export const ScheduleLeft = styled.div`
    // position: flex;
    // margin-right: 3%;
    margin-bottom: 10px;
    text-align: right;
    min-width: 278px;

    @media ${device.detailflexandsmaller} {
        text-align: left;
    }
    @media ${device.tabletandbigger} {
        margin-bottom: 24px;
    }
`;

export const ScheduleDetailItemTitleParagraph = styled.div`
    margin-top: 8px;
    margin-bottom: 20px;
    @media ${device.tabletandbigger} {
        margin-top: 24px;
    }
`;