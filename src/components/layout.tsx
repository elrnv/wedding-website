import * as React from 'react'
import type { HeadFC, PageProps } from "gatsby"
import { Link } from 'gatsby'
import Schedule from './schedule'
import Header from './header'
import Footer from './footer'
// import React, { useState, useEffect } from 'react'

import {
  Main,
  BackgroundImage,
  NavigationItem,
  SaveTheDate,
  HotelSnippet,
  AddressSection,
  Date,
  AddressLabel,
  Address,
  UpperContent,
  DateBanner,
  BACKGROUND_MAX
} from './styled'

const Layout = () => {
  const [bgopacity, setBgOpacity] = React.useState(1);
  const handleScroll = React.useCallback(() => {
    const x = window.scrollY;
    const a = 541;
    if (x < a) {
      const t = x / a - 1
      setBgOpacity(t * t)
    } else {
      setBgOpacity(0)
    }
  }, []);

  React.useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  const [windowSize, setWindowSize] = React.useState<{ width: number, height: number }>();

  const handleResize = React.useCallback(() => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  }, []);

  React.useEffect(() => {
    window.addEventListener('resize', handleResize, false);
    handleResize();
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  if (windowSize === undefined) {
    return <></>
  }

  const maxImgWidth = 1443;
  let imgWidth = maxImgWidth;
  if (windowSize.width < maxImgWidth) {
    imgWidth -= maxImgWidth - windowSize.width;
  }

  let posRelative = windowSize.width < BACKGROUND_MAX;

  const maxImgHeight = 963;
  const bgheight = imgWidth * maxImgHeight / maxImgWidth;

  const bgImage = (
    <BackgroundImage
      src="/dian-egor.png"
      $imageOpacity={bgopacity}
      $pos={posRelative}
      width={imgWidth}>
    </BackgroundImage>);

  const dateBanner = (
    <DateBanner $posRelative={posRelative}>
      <Date>
        JUNE 1st 2024 <br></br>
        TOFINO, BC
      </Date>
      <AddressSection>
        <AddressLabel
          href="https://www.wickinn.com/">Wickaninnish Inn</AddressLabel>
        <Address
          href="https://maps.app.goo.gl/3DMo5MGEchAzKUyb9">
          500 Osprey Lane
          <br />
          Tofino, BC VoR 2Zo
        </Address>
        {/* <Address
          href="https://maps.app.goo.gl/3DMo5MGEchAzKUyb9">
          
        </Address> */}
      </AddressSection>
    </DateBanner>)

  if (!posRelative) {
    return (
      <Main >
        <Header />
        <UpperContent $height={bgheight}>
          {bgImage}
          {dateBanner}
        </UpperContent>
        <Schedule />
        <Footer />
      </Main>
    )
  } else {
    return (
      <Main >
        <Header />
        <UpperContent $height={bgheight}>
          {bgImage}
        </UpperContent>
        {dateBanner}
        <Schedule />
        <Footer />
      </Main>
    )
  }
}

export default Layout
