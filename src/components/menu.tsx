import * as React from 'react'
import { Link } from 'gatsby'
import MenuIcon from './menu_icon'
import {
  NavigationItem,
  HorizontalMenu,
  HamburgerMenu,
  VerticalMenu,
  MENU_MAX,
} from './styled'


const Menu = () => {
  const [windowSize, setWindowSize] = React.useState<{ innerWidth: number, innerHeight: number }>();

  const handleWindowResize = React.useCallback(() => {
    const { innerWidth, innerHeight } = window;
    setWindowSize({ innerWidth, innerHeight });
  }, []);

  React.useEffect(() => {
    window.addEventListener('resize', handleWindowResize);
    handleWindowResize();
    return () => window.removeEventListener('resize', handleWindowResize);
  }, [handleWindowResize]);

  const [isMenuVisible, setMenuVisible] = React.useState(false);

  const toggleMenu = React.useCallback(() => {
    setMenuVisible(!isMenuVisible);
  }, [setMenuVisible, isMenuVisible]);

  if (windowSize === undefined) {
    return <></>
  }
  else if (windowSize.innerWidth > MENU_MAX) {
    return (
      <HorizontalMenu>
        <NavigationItem>
          <Link to="/" activeClassName="activeLink">
            S C H E D U L E
          </Link>
        </NavigationItem>
        <NavigationItem>
          <Link to="/qa" activeClassName="activeLink">
            Q & A
          </Link>
        </NavigationItem>
        <NavigationItem>
          <Link to="/travel" activeClassName="activeLink">
            T R A V E L
          </Link>
        </NavigationItem>
        <NavigationItem>
          <Link to="/activities" activeClassName="activeLink">
            A C T I V I T I E S
          </Link>
        </NavigationItem>
        <NavigationItem>
          <Link to="/photos" activeClassName="activeLink">
            P H O T O S
          </Link>
        </NavigationItem>
      </HorizontalMenu>
    )
  } else {
    return (
      <HamburgerMenu>
        <MenuIcon onClick={toggleMenu}></MenuIcon>
        <VerticalMenu $isVisible={isMenuVisible}>
          <NavigationItem>
            <Link to="/" activeClassName="activeLink">
              S C H E D U L E
            </Link>
          </NavigationItem>
          <NavigationItem>
            <Link to="/qa" activeClassName="activeLink">
              Q & A
            </Link>
          </NavigationItem>
          <NavigationItem>
            <Link to="/travel" activeClassName="activeLink">
              T R A V E L
            </Link>
          </NavigationItem>
          <NavigationItem>
            <Link to="/activities" activeClassName="activeLink">
              A C T I V I T I E S
            </Link>
          </NavigationItem>
          <NavigationItem>
            <Link to="/photos" activeClassName="activeLink">
              P H O T O S
            </Link>
          </NavigationItem>
        </VerticalMenu>
      </HamburgerMenu>
    )
  }
}

export default Menu