import * as React from 'react'

import {
  ScheduleDate,
  ScheduleTitle,
  ScheduleDetail,
  ScheduleDivider,
  ScheduleItemContainer,
  ScheduleContainer,

  ScheduleDetailTime,
  ScheduleDetailItem,
  ScheduleDetailItemTitle,
  ScheduleDetailItemParagraph,
  ScheduleDetailItemAddress,
  ScheduleRight,
  ScheduleLeft,
  ScheduleDetailItemTitleParagraph
} from './styled'

interface ScheduleItemProps {
  title: String;
  date: String;
  details: String;
}

interface ScheduleRightProps {
  title: String;
  addressTitle: String;
  addressLine1: String;
  addressLine2: String;
  attire: String;
  details: String;
  hasDetailsHeader?: Boolean;
  addressUrl: string;
}

interface ScheduleDetailsProps {
  time: String;
  title: String;
  addressTitle: String;
  addressLine1: String;
  addressLine2: String;
  attire: String;
  details: String;
  hasDetailsHeader?: Boolean;
  addressUrl: string;
}

const ScheduleItemMay30 = (props: ScheduleItemProps) => {
  const { title, date, details } = props;
  return (
    <div>
      <ScheduleItemContainer>
        <ScheduleTitle>{title}</ScheduleTitle>
        <ScheduleDate>{date}</ScheduleDate>
        <ScheduleDivider />
        <ScheduleDetailsComponent 
          time="AFTER 4:00PM"
          title="CHECK IN"
          addressTitle=""
          addressLine1=""
          addressLine2=""
          attire=""
          details=""
          addressUrl="https://maps.app.goo.gl/3UpDHNQPuKiiccKx9"
        />
      </ScheduleItemContainer>
    </div>
  )
}

const ScheduleItemMay31 = (props: ScheduleItemProps) => {
  const { title, date, details } = props;
  return (
    <div>
      <ScheduleItemContainer>
        <ScheduleTitle>{title}</ScheduleTitle>
        <ScheduleDate>{date}</ScheduleDate>
        <ScheduleDivider />
        <ScheduleDetailsComponent 
          time="11:00AM-12:00PM"
          title="HIKING THE RAINFOREST"
          addressTitle="Pacific Rim: Rainforest Trail"
          addressLine1="Tofino, BC V0R 2Z0"
          addressLine2=""
          attire="Casual / Athletic"
          details={
            "Immerse yourself in nature on a scenic hike. Towering red cedars and western hemlocks line the trail, with a lush bird canopy and soft, hanging moss. \n\nMeet us at the trailhead on the west side of the highway, as indicated on the map above. \n\nThe trail offers two loops, one on each side of the highway. Feel free to join us for one loop or conquer both! The combined length is 2.6km and takes approximately 45 minutes to complete."}
          addressUrl="https://maps.app.goo.gl/BfBgk8XyNLFb76E68"
        />
        <ScheduleDetailsComponent 
          time="2:30PM-6:00PM"
          title="WILDLIFE & WHALE WATCHING TOUR"
          addressTitle="Remote Passages Marine Excursions"
          addressLine1="51 Wharf Street"
          addressLine2="Tofino, BC V0R 2Z0"
          attire={
            "Casual / Athletic \n\nWe recommend bringing layers of warm clothing for the cool ocean breeze. You may also want a hat, sunglasses, sunscreen, and closed toed shoes."}
          details={
            "Please meet us at Meares Landing with Remote Passages by 2:30PM sharp. The boat will be leaving at 3:00PM. \n\nOur journey starts with a tranquil exploration of the east side of Meares Island's calm inlets. Keep your eyes peeled for playful sea otters, migrating shorebirds, and soaring bald eagles! We'll then navigate around the island, revealing breathtaking views, before venturing into the open waters where majestic grey whales may await. \n\nWe'll have two boats available for our tour, a comfortable cabin cruiser and an adventurous Zodiac. Both boats will depart and travel together. Guests with young children must be in the cabin cruiser."}
          addressUrl="https://maps.app.goo.gl/Lq5FDruvVfuC3gjR6"
        />
      </ScheduleItemContainer>
    </div>
  )
}

const ScheduleItemJune1 = (props: ScheduleItemProps) => {
  const { title, date, details } = props;
  return (
    <div>
      <ScheduleItemContainer>
        <ScheduleTitle>{title}</ScheduleTitle>
        <ScheduleDate>{date}</ScheduleDate>
        <ScheduleDivider />
        <ScheduleDetailsComponent 
          time="9:00AM-10:00AM"
          title="BEACH YOGA"
          addressTitle="Wick Inn: Chesterman Beach"
          addressLine1="500 Osprey Lane"
          addressLine2="Tofino, BC V0R 2Z0"
          attire={
            "Athletic \n\nWe recommend bringing a layer of warm clothing and water."}
          details={
            "Welcome the wedding day with movement and mindfulness! Join us for a rejuvenating morning yoga session led by Coastal Bliss. Yoga mats will be provided.\n\nTo ensure there are enough yoga mats for everyone, please let us know if you'd like to participate and have previously marked no on the interest form."}
          addressUrl="https://maps.app.goo.gl/YSW9qMkbkaM4cUt5A"
        />
         <ScheduleDetailsComponent 
          time="4:30PM-5:00PM"
          title="CEREMONY"
          addressTitle="Wick Inn: Rainforest Venue"
          addressLine1="500 Osprey Lane"
          addressLine2="Tofino, BC V0R 2Z0"
          attire={
            "Formal"}
          details={
            "Our ceremony will be held in the rainforest located outside of Wickaninnish Inn. The venue is accessible through a short and flat forest trail (approximately 2-4 minutes). Please see Q&A page for map to the start of the trail. Venue staff will be present to help with directions. \n\nPlease be seated at 4:30pm for the start of the ceremony. The area will open at 4:00pm. \n\nUmbrellas will be provided in case of light rain. The ceremony will move indoors to the Shoreline Terrace reception space in case of heavy storms."}
          addressUrl="https://maps.app.goo.gl/JpXSJR7SHFQnXGN49"
        />
        <ScheduleDetailsComponent 
          time="5:00PM-5:30PM"
          title="GROUP PHOTOS"
          addressTitle=""
          addressLine1=""
          addressLine2=""
          attire={
            ""}
          details={
            ""}
          addressUrl=""
        />
        <ScheduleDetailsComponent 
          time="6:00PM-6:30PM"
          title="CANAPES"
          addressTitle="Wick Inn: Shoreline Terrace"
          addressLine1="500 Osprey Lane"
          addressLine2="Tofino, BC V0R 2Z0"
          attire={
            ""}
          details={
            "Canapes will be provided for you at the Shoreline Terrace. Take a break, enjoy some tasty bites, and leave your mark in our guest book! We will be right back"}
          addressUrl="https://maps.app.goo.gl/86YV45aSpmhWED2Y9"
        />
          <ScheduleDetailsComponent 
          time="6:30PM"
          title="DINNER & CELEBRATIONS"
          addressTitle=""
          addressLine1=""
          addressLine2=""
          attire={
            ""}
          details={
            ""}
          addressUrl=""
        />
      </ScheduleItemContainer>
    </div>
  )
}

const ScheduleItemJune2 = (props: ScheduleItemProps) => {
  const { title, date, details } = props;
  return (
    <div>
      <ScheduleItemContainer>
        <ScheduleTitle>{title}</ScheduleTitle>
        <ScheduleDate>{date}</ScheduleDate>
        <ScheduleDivider />
        
        <ScheduleDetailsComponent 
          time="BEFORE 12:00PM"
          title="CHECK OUT"
          addressTitle=""
          addressLine1=""
          addressLine2=""
          addressUrl=""
          attire=""
          details=""
        />
      </ScheduleItemContainer>
    </div>
  )
}

export const ScheduleDetailsComponent = (props: ScheduleDetailsProps) => {
 
  
  // let timeSection;
  // if (time != "") {
  //   timeSection = {

  //   } = 
  // }

  return (
    <ScheduleDetail>
      <ScheduleLeft>
        <ScheduleDetailTime>
          {props.time}
        </ScheduleDetailTime>
      </ScheduleLeft>
      <ScheduleRightSection
        title = {props.title}
        addressTitle = {props.addressTitle}
        addressLine1 = {props.addressLine1}
        addressLine2 = {props.addressLine2}
        addressUrl = {props.addressUrl}
        attire = {props.attire}
        hasDetailsHeader = {true}
        details = {props.details}
      />
    </ScheduleDetail>
  )
}

export const ScheduleRightSection = (props: ScheduleRightProps) => {
  const { 
    title, 
    addressTitle,
    addressLine1,
    addressLine2,
    addressUrl,
    attire,
    hasDetailsHeader,
    details } = props;
  
  let addressSection;

  if (addressTitle != "") {
    addressSection = (
      <ScheduleDetailItemTitleParagraph>
        <ScheduleDetailItemTitle
          href={addressUrl}
          target="_blank"
        >
          LOCATION
        </ScheduleDetailItemTitle>
        <ScheduleDetailItemAddress 
          href={addressUrl}
          target="_blank"
        >
          {addressTitle}
        </ScheduleDetailItemAddress>
        <ScheduleDetailItemAddress 
          href={addressUrl}
          target="_blank"
        >
          {addressLine1}
        </ScheduleDetailItemAddress>
        <ScheduleDetailItemAddress
          href={addressUrl}
          target="_blank"
        >
          {addressLine2}
        </ScheduleDetailItemAddress>
      </ScheduleDetailItemTitleParagraph>
    )
  }

  let attireSection;
  if (attire != "") {
    attireSection = (
      <ScheduleDetailItemTitleParagraph>
        <ScheduleDetailItemTitle>
          ATTIRE
        </ScheduleDetailItemTitle>
        <ScheduleDetailItemParagraph>
          {attire}
        </ScheduleDetailItemParagraph>
      </ScheduleDetailItemTitleParagraph>
    )
  }

  let detailsHeaderSection;
  if (hasDetailsHeader) {
    detailsHeaderSection = (
      <ScheduleDetailItemTitle>
          DETAILS
      </ScheduleDetailItemTitle>
    )
  }
  let detailsSection;
  if (details != "") {
    detailsSection = (
      <ScheduleDetailItemTitleParagraph>
        {detailsHeaderSection}
        <ScheduleDetailItemParagraph>
          {details}
        </ScheduleDetailItemParagraph>
      </ScheduleDetailItemTitleParagraph>
    )
  }
  return (
    <ScheduleRight>
        <ScheduleDetailItem>
          {title}
        </ScheduleDetailItem>
        {addressSection}
        {attireSection}
        {detailsSection}
    </ScheduleRight>
  )
}

export const Schedule = () => {
  return (
    <ScheduleContainer>
      <ScheduleItemMay30
        title="ARRIVE"
        date="THURSDAY, MAY 30th, 2024"
        details="Checking in. Details coming soon"
      />
      <ScheduleItemMay31
        title="EXPLORE"
        date="FRIDAY, MAY 31st, 2024"
        details="Details coming soon"
      />      
      <ScheduleItemJune1
        title="CELEBRATE"
        date="SATURDAY, JUNE 1st, 2024"
        details="Details coming soon"
      />      
      <ScheduleItemJune2
        title="DEPART"
        date="SUNDAY, JUNE 2nd, 2024"
        details="Checking out. Details coming soon"
      />
    </ScheduleContainer>
  )
}

export default Schedule